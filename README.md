# Visualization in motion - *SwimFlow*
This is a repository for original codes of *SwimFlow* provided in the TVCG paper "Designing for Visualization in Motion: Embedding Visualizations in Swimming Videos". Interactive version of *SwimFlow* can be accessed at https://motion.isenberg.cc/Swimming/index.

## Getting started

To get started, either clone this repository, or if you stay within github, use the ''use this template'' function to generate your own version of this repository from which you can then edit. Then start a php server with the main directory of the repo as the root directory (so that index.html is at the base level), and you should be able to view the example project in your browser under an address like [localhost:8080/?debug].

Test your codes extensively. Locally, you can use a development environment such as MAMP to serve the *SwimFlow* pages and to test whether the tool works as it should. If you use MAMP, set the server directory in the preferences to the base directory of *SwimFlow*, that is, the level at which you can find this read.me file. Then start the server and open the page:

* http://localhost:8080/SwimFlow/index

in your browser (Please check if **YOUR GATE NUMBER** is 8080 or not. If not, replace 8080 by **YOUR GATE NUMBER**).

If you use a php server, please firstly CD to the director where you can find this readme file. Then enter

 * php -S localhost:8080

in the terminal. Same as before, change "8080" with **YOUR GATE NUMBER**. Now please enter the link mentioned above in your browser.

## Description
All participants' design data (in json format) will be recorded in "design" folder and their action flow (in csv format) will be recorded in "results" folder.

## Updating in progress
*SwimFlow* is currently under improvement. Please find the latest version here: https://aviz.gitlabpages.inria.fr/vis-in-motion-swimflow/.
Please note that such link might not work properly before the latest version is ready for release.

## Copyright & Citation
*SwimFlow* is released under CC-BY 4.0 license. 

To cite *SwimFlow* as a single tool: 

* *SwimFlow*, created on 2023.

To cite the entire paper:

* Lijie Yao, Romain Vuillemot, Anastasia Bezerianos, Petra Isenberg. Designing for Visualization in Motion: Embedding Visualizations in Swimming VideosTO APPEAR. IEEE Transactions on Visualization and Computer Graphics. To appear.

To cite the paper on **Visualization in motion**:

* Lijie Yao, Romain Vuillemot, Anastasia Bezerianos, Petra Isenberg. Visualization in motion: A Research Agenda and Two Evaluations. IEEE Transactions on Visualization and Computer Graphics, vol. 28, no. 10, pp. 3546-3562, 1 Oct. 2022, doi: 10.1109/TVCG.2022.3184993.
